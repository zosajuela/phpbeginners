<?php

echo "hello world </br>";

print "goodbye!   ";

#print & return is use to return the function
#<?= "helloworld";
echo "<br/>";
//set variable
//data types
$var1 = "how are you   ";
$var_num = 19;
$float = 92.3;
$boolean = true;
$array = array("isip", 20, 80.5, true, "panaginip");

echo $var1;
echo "<br/>";
echo $var_num;


var_dump($var_num);
echo "<br/>";
var_dump($array);

// concatnate
echo $var1.$var_num; or echo $var." ".$var_num

//METHOD
// echo strpos($var1); //count the number of strings
// echo strlen($var1); //count the number of length
//echo str_replace(" ","", $text); //remove the space of the $text variable
//$old = str_replace(" ","",$text); //repackage the $text with a function str_replace
//echo strlen($old); //count the new length of the $text

// strtolower == to lower case
// strtoupper == to upper case

//CONSTANTS Value just like const
// define("Pokemon", "Pikachu");
// echo Pokemon; //Pokemon should be used once and cannot be change
// define("Pokemon", "Pikachu", true); add true to add case sensitive


//Mathmatical operators
// $nine = 9;
// $four = 4;
// echo $nine + $four;  echo $nine * $four; 
//echo $nine ** $four; ** <= 9 to the power of 4

//OPERATORS

// IF STATEMENT
// if($number === $number2){
//     echo "True";
// }else{
//     echo "False";
// }
// $mango = "yellow";
// $size = "big";  //AND or && can be use // OR or || //xor means either of them
// if($mango == "yellow" AND $size == "big"){
//     echo "true";  
// } else{
//     echo "False";
// }

//SWITCH
// switch($mango){
//     case "yellow":
//         echo "hinog na manga";
//          break;
//     case: "green":
//         echo "its not ripe man!";
//           break;
//     default:
//         echo "no seedlings";
// }

//LOOP (excecutes once)
// while($x <= 10){
//     echo "ang bilang nato ay ".$x. "<br>";
//     $x++
// }

//DO WHILE (excecutes once)

// do{
//     echo "Ang Bilang nato ay".$x. "<br/>";
//     $x++;
// }while($x <= 10);


// for($i = 0; $i <= 5; $i++){
//     echo "eto ay pang" .$i. "<br/>";
// }
// foreach($data_types as $values){
//     echo $values."<br/>";
// }

// gettype($name) <--get the type of variable (this will return the word string )
//isset($name) <- check if its true
// define('PI'.'<br>');