<!-- 
$a = 3;
$b = 5;
$c = 7;

echo ($a + $b) * $c.'<br>'; //() will add a+b first and then times $c -->
<!-- echo $a + $b * $c.'<br>'; this will times b and c first and then add $a -->

<!-- $a += $b; echo $a; will automatically add a + b with out asigning
$e = $a + $b; echo $e; this will add a and b and stored on $e -->

<!-- $a++; a is increased by 1 return 5
++$a; existing a which is 5 and then added then existing returning 7 -->
<!-- 
is_float(1.25); true
is_double(1.25);true
is_int(5);true
is_numeric("3.45"); true
is_numeric("3g.45"); fa;se -->
<!-- 
$strNumber = '12.34';
$number = (float)$strNumber;
var_dump($number);
float_val($number); will convert this into string.

math method
abs(-15) return 15 (absolute)
pow(2,3) return pow(power) 2x2x2  = 8
sqrt(15) sqrt(16) squarot 16 = 4
max(2,9,3); get the max number which is 9 is the return
min(2,3); get the min number which is 2 is the return
round(2.4) return 2
round(2.6) return 3
floor(2.4) return 2
ciel(2.4) return 3 -->
<!-- 
$number = 123456789.12345;
echo number_format($number,2); -->

STRING MANIPULATION
strlen($string); coutns number of length of the string
strcspn("Hello world!","w",3,6); this returns 3 // The start position is 0 and the length of the search string is 6.
strpbrk("hellow world!","o") case sensitive return ow world!
strrchr("hello world", "world!" ) case sensitive  returns world! (no true or false)
strstr("Hello world! obama","world",false); returns world! obama if false, Hello if true 
stristr("Hello world!","WORLD"); Find the first occurrence of "world" inside "Hello world!", and return the rest of the string:
strspn("abcdefand","abc"); case sensitive returns number 3 since all letters and inside 1st params
strtr("Hilla Warld","ia","eo"); case sensitive return Hello World. replace the 1st param's, 2nd param replace by 3rd param
substr("Hello world", -1) returns d. if -1 is 5 returns  world
substr_replace("Hello world","earth",6); replace the 6th length and then changed it to earth
echo substr_compare("world","rl",1,2); returns -3 if rl changed to "or",1,2 it will return 0
ucfirst('hello') convert first letter to uppercase Hello
lcfirst('HELLO') convert first letter to lowercase
ucwords('hello how are you?') convert every first letter to upper )Hello How Are You
strpos($string, 'world') case sensitive return a number specific to the position of the first word
stripos($string, world) case insensitive) return a number specific to the position of the first word
strchr("Hello world!","world", false); returns world! if true returns Hello
substr($string, 8) returns the 8th letter onwards return rawurldecode
str_replace("world","Peter","Hello world!"); replace the word world with Peter 
str_ireplace("WORLD","Peter","Hello world!"); Replace the characters "WORLD" (case-insensitive) with "Peter":
trim($string) remove spaces on both session_destroy
ltrim($string) remove spaces from left side
rtrim($string) remove spaces from right side
str_word_count($string) count words
strrev($string) reverse string
strtoupper($string) to uppercase
strtolower($string) to lower case
wordwrap($str,26,"<br>\n"); wrap word by 25px maybe or em

 addcslashes($str,'m')."<br>"; $str = "humble Homepage!"; adds \ before all letter m
 stripslashes() function removes backslashes added by the addslashes() function.
 bin2hex("Hello World!"); convert string to hexadecimal value (hasher)
 sha1($str) or sha1_file(); convert string to hexadecimal value (hasher)
pack($string) convert back from being a hexadcimal value (unhash)
chop($string, world) removes the word world if its the last rightside
chunk_split($str,6,"..."); split the string starting from 6 then add ...
    REGEX
preg_replace($pattern, 'replced', $str); $str = "visit microsoft website to learn microsoft"; $pattern = '/microsoft/i'; return will be removing microsoft and replace it with the word replced
preg_filter('/[0-9]+/', '($0)', $input); only returns the sentences that dont have numbers and for with 0's wraps in ()
preg_grep("/^p/i", $input); returns only those objects with the letter P 
preg_match($pattern, $str); $str = "visit W3schools"; $pattern = "/W3schools/i";  if patter match with str return 1 else 0
preg_match_all($pattern, $str, $matches); 1st param is main 2nd param is comparison 3rd param is result return all matches and count them like an array
preg_split($pattern, $date) return segragate the date according to its pattern - and list it like an array

explode($str); returns array ex. Array ( [0] => Hello [1] => world. [2] => It's [3] => a [4] => beautiful [5] => day. )
joing("",$str) join's arrays
money_format($number) 1,234.56
nl2br($str) 
str_split("Hello",3); Array ( [0] => Hel [1] => lo )
str_getcsv()  parses a string for CSV format and returns an array containing the fields read.
str_pad($str,20,".",STR_PAD_LEFT) or str_pad($str,20,".:",STR_PAD_BOTH); return $str = "......Hello World"; & ....Hello world....
str_repeat("Wow",13); repeats Wow 13 times
str_rot13("Hello Worlds"); return Uryyb Jbeyqf
str_shuffle("Hello World"); return random shuffle of letters

strcasecmp($str1,$str2); case-insensitive compare both strings and return a number 0 if exact. 
strcmp("Hello world!","Hello world!"); case-sensitive returns 0 if match
strncmp("Hello","hELLo",6) same strcmp()except that strcmp() does not have the length.
strnatcasecmp($str1, $str2); case sensitive campares 2 strings return -1 1 and 0 if match
strncasecmp($str1, $str2); case insensitive campares 2 strings return -1 1 and 0 if match
strip_tags("Hello <b><i>world!</i></b>","<b>"); inputs an html element can be styled with class or id
<!-- 
https://support.squarespace.com/hc/en-us/articles/206545567-Using-the-CSS-Editor#toc-add-css-code -->
<!-- https://regex101.com/ -->

sscanf()
quoted_printable_decode()
quoted_printable_encode()
strcoll()
substr_count()

error codes preg_last_error() https://www.w3schools.com/php/func_regex_preg_last_error.asp