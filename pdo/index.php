<?php

// print_r(PDO::getAvailableDrivers());
$host = "localhost";
$user = "root";
$password = "Oliver080";
$dbname = "student_management_system";

$dsn = "mysql:host=$host;dbname=$dbname";

$pdo = new PDO($dsn, $user, $password);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

$stmt = $pdo->query("SELECT * FROM students_list");

// while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
//     echo $row['first_name']." ".$row['last_name']."<br/>";
// }

// while($row = $stmt->fetch(PDO::FETCH_OBJ)){
//     echo $row->first_name." ".$row->last_name."<br/>";
// }

//prepared statements

// $sql = "SELECT * FROM userdata WHERE first_name = '$input'";

// $gender = "male";

// $sql = "SELECT * FROM userdata WHERE gender = ?";
// $stmt = $pdo->prepare($sql);
// $stmt->execute([$gender]);
// $users = $stmt->fetchAll();

// foreach($users as $user){
//     echo $user->first_name." ".$user->last_name. "<br/>";
// }

//NAMED PARAMETER
// $gender = "female";

// $sql = "SELECT * FROM students_list WHERE gender = :gender";
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['gender' => $gender]);
// $users = $stmt->fetchAll();

// foreach($users as $user){
//     echo $user->first_name." ".$user->last_name."-".$user->gender."<br/>";
// }

//QUERY SINGLE ITEM

// $id = 5;

// $sql = "SELECT * FROM students_list WHERE id = :id";
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['id' => $id]);
// $user = $stmt->fetch();

// echo $user->first_name." ".$user->last_name;

// getting number of rows count the number of male or female
// $gender = "male"; // or "female";

// $sql = "SELECT * FROM students_list WHERE gender = :gender";
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['gender' => $gender]);
// $users = $stmt->fetchAll();
// $userCount = $stmt->rowCount();

// echo $userCount;
// $firstname = "Gary";
// $lastname = "Valencia";
// $gender = "male";
// $birthday = "05/02/1995";
// $id = 507;
//INSERT
// $sql = "INSERT INTO students_list(first_name,last_name,gender,birth_day)VALUE(:first_name,:last_name,:gender,:birth_day)";
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['first_name' => $firstname, 'last_name' => $lastname, 'gender' => $gender, 'birth_day' => $birthday]);
// echo $stmt->rowCount();

//UPDATE
// $sql = "UPDATE students_list SET first_name = :first_name WHERE birth_day = :birth_day";
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['first_name' => $firstname, 'birth_day' => $birthday]);
// echo $stmt->rowCount();

// DELETE

// $sql = "DELETE FROM students_list WHERE id = ?";
// $stmt = $pdo->prepare($sql);
// $stmt->execute([$id]);
// echo $stmt->rowCount();

//SEARCH
$search = "%ue%";
$sql = "SELECT * FROM students_list WHERE last_name LIKE ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$search]);
$users = $stmt->fetchAll();
$userCount = $stmt->rowCount();

foreach($users as $user){
    echo $user->first_name." ".$user->last_name."-".$user->gender."<br/>";
}
?>